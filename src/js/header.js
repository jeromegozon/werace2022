;(function(win, doc, undefined) {
    'use strict';
    
    var SHRINK_CLASS = 'shrink';

    var Header = function(headerSelector) {
        var _ = this;
        _.$header = (typeof headerSelector === 'string') ? doc.querySelector(headerSelector) : headerSelector
        _.isShrunk = false;
        _.scrollShrinkTarget = 200;
        _.init();
    }
    
    Header.prototype.init = function() {
        var _ = this;
        _.checkShrink();
        _.initEvents();
    }

    Header.prototype.initEvents = function() {
        var _ = this;
        win.addEventListener('scroll', _.checkShrink.bind(_));
    }

    Header.prototype.checkShrink = function() {
        var _ = this;
        if(win.scrollY > _.scrollShrinkTarget) {
            _.shrink();
        } else {
            _.unShrink();  
        }
    }
    
    Header.prototype.shrink = function() {
        var _ = this;
        if(_.isShrunk) return;
        _.isShrunk = true;
        _.$header.classList.add(SHRINK_CLASS);
    }

    Header.prototype.unShrink = function() {
        var _ = this;
        if(!_.isShrunk) return;
        _.isShrunk = false;
        _.$header.classList.remove(SHRINK_CLASS);
    }

    win.Header = Header;

})(window, document);