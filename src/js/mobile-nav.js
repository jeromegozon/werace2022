;(function(win, doc, undefined) {
    'use strict';
    
    var DRAWER_SEL              = '.navig--mobile__drawer';
    var OVERLAY_SEL             = '.navig--mobile__overlay';
    var BODY_NAVIG_OPEN_CLASS   = 'body-navig--is-open';
    var OPEN_CLASS              = 'navig--mobile--is-open';
    var HIDDEN_CLASS            = 'navig--mobile--is-hidden';
    var STRING                  = 'string';

    var $body = doc.querySelector('body');

    var MobileNav = function(mobileNavSelector, opts) {
        var _ = this;
        opts = opts || {};
        _.$mobileNav = (typeof mobileNavSelector === STRING) ? doc.querySelector(mobileNavSelector) : mobileNavSelector
        _.$burger = (typeof opts.burgerEl === STRING) ? doc.querySelector(opts.burgerEl) : opts.burgerEl
        _.$overlay = _.$mobileNav.querySelector(OVERLAY_SEL);
        _.$drawer = _.$mobileNav.querySelector(DRAWER_SEL);
        _.$navWrap = _.$mobileNav.querySelector('nav');
        _.$links = _.$navWrap.querySelectorAll('a');
        _.isOpen = false;
        _.init();
    }
    
    MobileNav.prototype.init = function() {
        var _ = this;
        _.initEvents();
    }

    MobileNav.prototype.initEvents = function() {
        var _ = this;
        var click = 'click';
        _.$overlay.addEventListener(click, _.close.bind(_));
        _.$burger && _.$burger.addEventListener(click, _.open.bind(_));
        _.$drawer.addEventListener('transitionend', _.onDrawerTransEnd.bind(_));
        _.initLinksClick();

    }

    MobileNav.prototype.open = function() {
        var _ = this;
        _.$mobileNav.classList.remove(HIDDEN_CLASS);
        setTimeout(function() {
            $body.classList.add(BODY_NAVIG_OPEN_CLASS);
            _.$mobileNav.classList.add(OPEN_CLASS);
            _.isOpen = true;
        }, 100);
    }

    MobileNav.prototype.close = function() {
        var _ = this;
        $body.classList.remove(BODY_NAVIG_OPEN_CLASS);
        // _.$mobileNav.classList.add(HIDDEN_CLASS);
        _.$mobileNav.classList.remove(OPEN_CLASS);
        _.isOpen = false;
    }

    MobileNav.prototype.initLinksClick = function() {
        var _ = this;
        for(var i =0; i < _.$links.length; i++) {
            _.$links[i].addEventListener('click', function(e) {
                e.preventDefault();
                _.close();
            });
        }
    }

    MobileNav.prototype.onDrawerTransEnd = function() {
        var _ = this;
        setTimeout(function() {
            if(!_.isOpen) {
                _.$mobileNav.classList.add(HIDDEN_CLASS);
            }
            _.isOpen = !_.isOpen;
        }, 500);
    }

    win.MobileNav = MobileNav;

})(window, document);