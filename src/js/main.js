;(function(win, doc, undefined) {
    'use strict';

    var LOAD    = 'load';
    var RESIZE  = 'resize';
    
    var $body = doc.querySelector('body');

    function initAOS() {
        if(typeof AOS != 'undefined') {
            AOS.init({
                easing: 'ease',
                duration: 1000
            });
        }
        win.addEventListener(LOAD, function() {
            initAOS();
        });
    }

    function removeFullPageLoading() {
        var $fullPageLoading = doc.querySelector('#fullpage-loading');
        if(!$fullPageLoading) return;
        setTimeout(function() {
            $fullPageLoading.classList.add('hide');
            $body.classList.remove('overflow-hidden');

            setTimeout(function() {
                initAOS();
            }, 1000);

            setTimeout(function() {
                $fullPageLoading.parentNode.removeChild($fullPageLoading);
            }, 2000);
        }, 1000);
    }

    function curveHeroContestDate() {
        var $contestDate = doc.querySelector('.hero-contest-date');
        var circleType = new CircleType($contestDate)
            .dir(-1);

        function updateRadius() {

            if(win.outerWidth >= 3000) {
                circleType.radius(3400);
            }
            else if(win.outerWidth >= 1900) {
                circleType.radius(2200);
            }
            else if(win.outerWidth >= 1400) {
                circleType.radius(1900);
            }
            else if(win.outerWidth >= 1200) {
                circleType.radius(1600);
            }
            else if(win.outerWidth >= 992) {
                circleType.radius(1400);
            }
            else if(win.outerWidth >= 576) {
                circleType.radius(1200);
            } 
            else {
                circleType.radius(700);
            }
        }
        win.addEventListener(RESIZE, updateRadius);
        win.addEventListener(LOAD, updateRadius);
        updateRadius();

    }

    function initSectionPrize() {
        var $secAmazingPrize = doc.querySelector('.section--prizes__amazing');
        var $secPrizesGroup = doc.querySelector('.section__inner-group');
        var px = 'px';

        function setMarginPaddingTop() {
            var margTop = ($secAmazingPrize.clientHeight / 2);
            $secPrizesGroup.style.marginTop = (margTop * -1) + px; // set negative margin top
            $secPrizesGroup.style.paddingTop = margTop + px; // set positive margin top
        }
        
        if(!$secAmazingPrize || !$secPrizesGroup) return;
        win.addEventListener(RESIZE, setMarginPaddingTop);
        win.addEventListener(LOAD, setMarginPaddingTop);
        setMarginPaddingTop();
    }

    function init() {

        new ResponsiveAutoHeight('.js-info-auto-height');
        new ResponsiveAutoHeight('.js-hero-btn-auto-height');
        new Header('#header');
        new MobileNav('.navig--mobile', {
            burgerEl: '.burger'
        });
        
        // remove fullpage loading after the `section--hero` images have loaded
        var imgLoad = imagesLoaded(doc.querySelector('.section--hero'));
        imgLoad.on('done', removeFullPageLoading);
        imgLoad.on('fail', function() {
            console.log('failed to load images');
        });

        curveHeroContestDate();

        initSectionPrize();

    }

    doc.addEventListener('DOMContentLoaded', init);

})(window, document);