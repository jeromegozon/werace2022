var SectionScroller = (function(win, doc, undefined) {
    'use strict';
    
    var requestAnimationFrame = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame;

    function easeOutQuart(t) {
        return 1-(--t)*t*t*t;
    }
    function easeInOutSmoother(t) {
        var ts = t * t, tc = ts * t; 
        return 6*tc*ts - 15*ts*ts + 10*tc;
    }

    function scrollTo(to, duration) {
        var start = window.scrollY || window.pageYOffset
        var time = Date.now()
        var duration = duration || Math.abs(start - to) / 3;

        (function step() {
            var dx = Math.min(1, (Date.now() - time) / duration)
            var pos = start + (to - start) * easeInOutSmoother(dx)

            window.scrollTo(0, pos)

            if (dx < 1) {
                requestAnimationFrame(step)
            }
        })();
    }

    function initEvents() {

        // attach click event to elements with `.js-scrollto` classes
        var $scrollLinks = doc.querySelectorAll('.js-scrollto'); 
        for(var i = 0; i < $scrollLinks.length; i++) {
            $scrollLinks[i].addEventListener('click', function(e) {
                e.preventDefault();
                var target = this.getAttribute('data-target');
                var $target = doc.querySelector(target);
                $target && scrollTo($target.offsetTop - 55, 1000);
            });
        }

        // attach click event to elements with `.js-scrollto--top` classes
        var $scrollLinks = doc.querySelectorAll('.js-scrollto--top'); 
        for(var i = 0; i < $scrollLinks.length; i++) {
            $scrollLinks[i].addEventListener('click', function(e) {
                e.preventDefault();
                scrollTo(0, 2000);
            });
        }

    }
    initEvents();

    return {
        scrollTo: scrollTo
    }

})(window, document);