const { series, parallel, src, dest, watch } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const plumber = require('gulp-plumber');
const del = require('del');
const prefix = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const gulpif = require('gulp-if');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync').create();
const nunjucksRender = require('gulp-nunjucks-render');
const htmlbeautify = require('gulp-html-beautify');


const basePath = {
    src: 'src/',
    dest: 'dist/'
};

let isProd = false;
let initBrowserSync = false;

const srcAssets = {
    styles: basePath.src + 'scss/',
    scripts: basePath.src + 'js/',
    copy_styles: basePath.src + 'copy_css/',
    copy_scripts: basePath.src + 'copy_js/',
    images: basePath.src + 'img/',
    videos: basePath.src + 'videos/',
    icons: basePath.src + 'icons/',
    fonts: basePath.src + 'fonts/',
    vendors: basePath.src + 'vendors/',
    css: basePath.src + 'css/',
    js: basePath.src + 'js/',
    modules: 'node_modules/',
    root: basePath.src
};

const destAssets = {
    styles: basePath.dest + 'css/',
    scripts: basePath.dest + 'js/',
    images: basePath.dest + 'img/',
    videos: basePath.dest + 'videos/',
    icons: basePath.dest + 'icons/',
    fonts: basePath.dest + 'fonts/',
    vendors: basePath.dest + 'vendors/',
    css: basePath.dest + 'css/',
    js: basePath.dest + 'js/',
    root: basePath.dest
};

const scriptFiles = [
    srcAssets.scripts + 'header.js',
    srcAssets.scripts + 'mobile-nav.js',
    srcAssets.scripts + 'section-scroller.js',
    srcAssets.scripts + 'main.js',
]
const vendorScriptFiles = [
    srcAssets.scripts + 'vendors/polyfills/foreach.js',
    srcAssets.scripts + 'vendors/responsive-auto-height.min.js',
    srcAssets.scripts + 'vendors/imagesloaded.pkgd.min.js',
    srcAssets.scripts + 'vendors/circletype.min.js',
    srcAssets.scripts + 'vendors/gg-modal.min.js'
]

const sassProdOutput = { outputStyle: 'compressed' };

async function setInitBrowserSync() {
    initBrowserSync = true;
}
async function setProd() {
    console.log('############ =========> Building files for Production');
    isProd = true;
}

async function clean() {
    del.sync(destAssets.root);
}

async function styles() {
    src(srcAssets.styles + 'main.scss')
        .pipe(plumber())
        .pipe(sass(isProd ? {...sassProdOutput} : null))
        .pipe(prefix())
        .pipe(dest(destAssets.styles))
        .pipe(browserSync.reload({
            stream: true
        }))
}
async function vendorStyles() {
    src(srcAssets.styles + 'vendors.scss')
        .pipe(plumber())
        .pipe(sass({...sassProdOutput}))
        .pipe(prefix())
        .pipe(dest(destAssets.styles))
        .pipe(browserSync.reload({
            stream: true
        }));
}

async function scripts() {
    src(scriptFiles)
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(gulpif(isProd, uglify()))
        .pipe(dest(destAssets.scripts));
}
async function vendorScripts() {
    src(vendorScriptFiles)
        .pipe(plumber())
        .pipe(concat('vendors.js'))
        .pipe(uglify())
        .pipe(dest(destAssets.scripts));
}

async function copyScripts() {
    src(srcAssets.copy_scripts + '**/*')
        .pipe(plumber())
        .pipe(dest(destAssets.scripts))
}
async function copyStyles() {
    src(srcAssets.copy_styles + '**/*')
        .pipe(plumber())
        .pipe(dest(destAssets.styles))
}

async function fonts() {
    src(srcAssets.fonts + '**/*')
        .pipe(plumber())
        .pipe(dest(destAssets.fonts))
}
async function images() {
    src(srcAssets.images + '**/*')
        .pipe(plumber())
        .pipe(dest(destAssets.images))
}
async function videos() {
    src(srcAssets.videos + '**/*')
        .pipe(plumber())
        .pipe(dest(destAssets.videos))
}
async function icons() {
    src(srcAssets.icons + '**/*')
        .pipe(plumber())
        .pipe(dest(destAssets.icons))
}
async function vendors() {
    src(srcAssets.vendors + '**/*')
        .pipe(plumber())
        .pipe(dest(destAssets.vendors))
}
async function html() {
    src(srcAssets.root + '*.html')
        .pipe(plumber())
        .pipe(dest(destAssets.root))
}

async function template() {
    src(srcAssets.root + 'template/pages/**/*.+(html|nunjucks|njk)')
        .pipe(plumber())
        .pipe(nunjucksRender({
            path: [srcAssets.root + 'template/']
        }))
        .pipe(htmlbeautify())
        .pipe(dest(destAssets.root))
};


async function browserSyncInit() {
    if(isProd || !initBrowserSync) return;
    browserSync.init({
        notify: false,
        server: {
            baseDir: destAssets.root,
        }
    });
}

async function browserSyncReload() {
    browserSync.reload();
}


async function watcher() {
    if(isProd || !initBrowserSync) return;
    watch(srcAssets.root + '*.html', series(html, browserSyncReload));
    watch(srcAssets.images + '**/*', series(images));
    watch(srcAssets.videos + '**/*', series(videos));
    watch(srcAssets.vendors + '**/*', series(vendors));
    watch(srcAssets.fonts + '**/*', series(fonts));
    watch(srcAssets.icons + '**/*', series(icons));
    watch(srcAssets.styles + '**/*.+(scss|sass|css)', series(styles, browserSyncReload));
    watch(srcAssets.scripts + '**/*.js', series(scripts, browserSyncReload));
    watch(srcAssets.copy_scripts + '**/*.js', series(copyScripts, browserSyncReload));
    watch(srcAssets.copy_styles + '**/*.css', series(copyStyles, browserSyncReload));
    watch(srcAssets.root + '**/*.+(html|nunjucks|njk)', series(template, browserSyncReload));
}

exports.build = series(
    clean,
    parallel(
        styles,
        vendorStyles,
        scripts,
        vendorScripts,
        copyStyles,
        copyScripts
    ),
    fonts,
    images,
    videos,
    icons,
    html,
    template,
    watcher,
    browserSyncInit
);


exports.default = series(
    setInitBrowserSync,
    exports.build
);
exports.production = series(
    setProd,
    exports.build
);

exports.clean = clean;